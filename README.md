# Angular Project README

## Project Overview

This project is an Angular application for managing delivery services. It allows users to view available time slots, filter them based on delivery method, and manage bookings.


## Technologies Used

- Angular 18: Frontend framework for building the user interface.
- TypeScript: Programming language used in Angular applications.
- HTML/CSS: Markup and styling for the frontend.
- Bootstrap: Frontend framework for responsive design and UI components.
- RxJS: Library for reactive programming.
- Angular CLI: Command-line interface for Angular development tasks.

## Setup Instructions

1. Clone the repository to your local machine:

   ``` git clone https://gitlab.com/carrefour-demo/delivery-front.git ```

2. Navigate to the project directory:

   ``` cd delivery-front ```

3. Install dependencies: ( make sure you are using angular cli 18)

   ``` npm install ```

4. Run the development server:

   ``` ng serve ```

5. Access the application in your browser:

   ``` http://localhost:4200 ```

6. **Log In with Test User Credentials:**

  - **Username**: `test`
  - **Password**: `0000`
