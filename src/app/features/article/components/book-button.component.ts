import {
  Component,
  DestroyRef,
  EventEmitter,
  inject,
  Input,
  Output,
} from "@angular/core";
import { Router } from "@angular/router";
import { NgClass } from "@angular/common";
import { TimeSlotsService } from "../services/time-slots.service";
import { UserService } from "../../../core/auth/services/user.service";
import { TimeSlot } from "../models/time-slot.model";

@Component({
  selector: "app-book-button",
  template: `
    <button
      class="btn btn-sm btn-outline-primary"
      [ngClass]="{disabled: isSubmitting }"
      (click)="bookTimeSlot()"
    > BOOK <ng-content></ng-content>
    </button>
  `,
  imports: [NgClass],
  standalone: true,
})
export class BookButtonComponent {
  destroyRef = inject(DestroyRef);
  isSubmitting = false;

  @Input() timeSlot!: TimeSlot;
  @Input() deliveryMethod!: string;
  @Output() booked = new EventEmitter<TimeSlot>();

  constructor(
    private readonly timeSlotService: TimeSlotsService,
    private readonly router: Router,
    private readonly userService: UserService,
  ) {
  }

  bookTimeSlot(): void {
    this.isSubmitting = true;

   this.timeSlotService.book(this.timeSlot.id, this.deliveryMethod)
      .subscribe({
        next: () => {
          this.isSubmitting = false;
          this.booked.emit(this.timeSlot);
        },
        error: () => (this.isSubmitting = false),
      });
  }

}
