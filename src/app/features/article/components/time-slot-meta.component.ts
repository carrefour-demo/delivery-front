import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { TimeSlot } from "../models/time-slot.model";
import { RouterLink } from "@angular/router";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-time-slot-meta",
  template: `
    <div class="article-meta">


      <div class="info">
        <a class="author">
          {{ timeSlot.startTime | date: "dd/MM/yyyy HH:mm:ss" }}
        </a>
        <span class="date">
          {{ timeSlot.endTime | date: "dd/MM/yyyy HH:mm:ss" }}
        </span>
      </div>

      <ng-content></ng-content>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [RouterLink, DatePipe],
  standalone: true,
})
export class TimeSlotMetaComponent {
  @Input() timeSlot!: TimeSlot;
}
