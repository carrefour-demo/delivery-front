import {Component, EventEmitter, Input, Output} from "@angular/core";
import { TimeSlot } from "../models/time-slot.model";
import { TimeSlotMetaComponent } from "./time-slot-meta.component";
import { RouterLink } from "@angular/router";
import { NgForOf } from "@angular/common";
import { BookButtonComponent } from "./book-button.component";

@Component({
  selector: "app-time-slot-preview",
  template: `
    <div class="article-preview">
      <app-time-slot-meta [timeSlot]="timeSlot">
        <app-book-button
          [timeSlot]="timeSlot"
          (booked)="bookTimeSlot($event)"
          [deliveryMethod]= "deliveryMethod"
          class="pull-xs-right">
        </app-book-button>
      </app-time-slot-meta>
    </div>
  `,
  imports: [TimeSlotMetaComponent, BookButtonComponent, RouterLink, NgForOf],
  standalone: true,
})
export class TimeSlotPreviewComponent {
  @Input() timeSlot!: TimeSlot;
  @Input() deliveryMethod!: string;
  @Output() booked = new EventEmitter<TimeSlot>();

  bookTimeSlot(timeSlot: TimeSlot): void {
    this.booked.emit(timeSlot);
  }
}
