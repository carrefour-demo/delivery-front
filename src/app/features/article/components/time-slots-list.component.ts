import { Component, DestroyRef, inject, Input } from "@angular/core";
import { TimeSlotsService } from "../services/time-slots.service";
import { TimeSlotListConfig } from "../models/time-slot-list-config.model";
import { TimeSlot } from "../models/time-slot.model";
import { TimeSlotPreviewComponent } from "./time-slot-preview.component";
import { NgClass, NgForOf, NgIf } from "@angular/common";
import { LoadingState } from "../../../core/models/loading-state.model";
import { takeUntilDestroyed } from "@angular/core/rxjs-interop";

@Component({
  selector: "app-time-slots-list",
  template: `
    @if (loading === LoadingState.LOADING) {
      <div class="article-preview">Loading time slots...</div>
    }

    @if (loading === LoadingState.LOADED) {
      @for (timeSlot of results; track timeSlot.id) {
        <app-time-slot-preview [timeSlot]="timeSlot"
        [deliveryMethod]="deliveryMethod"
        (booked)="timeSlotBooked($event)"/>
      } @empty {
        <div class="article-preview">No time slots available ...</div>
      }

      <!-- <nav>
        <ul class="pagination">
          @for (pageNumber of totalPages; track pageNumber) {
            <li
              class="page-item"
              [ngClass]="{ active: pageNumber === currentPage }"
            >
              <button class="page-link" (click)="setPageTo(pageNumber)">
                {{ pageNumber }}
              </button>
            </li>
          }
        </ul>
      </nav> -->
    }
  `,
  imports: [TimeSlotPreviewComponent, NgForOf, NgClass, NgIf],
  standalone: true,
})
export class TimeSlotsListComponent {
  query!: TimeSlotListConfig;
  results: TimeSlot[] = [];
  currentPage = 1;
  totalPages: Array<number> = [];
  loading = LoadingState.NOT_LOADED;
  LoadingState = LoadingState;
  destroyRef = inject(DestroyRef);

  @Input() deliveryMethod!: string;
  @Input() size!: number;
  @Input()
  set config(config: TimeSlotListConfig) {
    if (config) {
      this.query = config;
      this.currentPage = 1;
      this.runQuery();
    }
  }

  constructor(private timeSlotsService: TimeSlotsService) {}

  /*
  setPageTo(pageNumber: number) {
    this.currentPage = pageNumber;
    this.runQuery();
  } */

  runQuery() {
    this.loading = LoadingState.LOADING;
    this.results = [];

    if (this.size) {
      this.query.filters.size = this.size;
      this.query.filters.page = this.size * (this.currentPage - 1);
    }

    this.timeSlotsService
      .query(this.query)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((data) => {
        this.loading = LoadingState.LOADED;
        this.results = data;

        this.totalPages = Array.from(
          new Array(Math.ceil(data.length / this.size)),
          (val, index) => index + 1,
        );
      });
  }

  timeSlotBooked(timeSlot : TimeSlot) {
    const index = this.results.indexOf(timeSlot);
    if (index !== -1) {
      this.results.splice(index, 1);
    }
  }
}
