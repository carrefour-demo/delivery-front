export interface EntityModel<T> {
  _embedded:
    { stringList: [T]};
  _links: {
    self: {
      href: string;
    };
  };
}
