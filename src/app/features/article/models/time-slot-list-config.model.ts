export interface TimeSlotListConfig {
  filters: {
    deliveryMethod?: string;
    page?: number;
    size?: number;
  };
}
