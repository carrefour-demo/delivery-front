
export interface TimeSlot {
  id: number;
  startTime: string;
  endTime: string;
  deliveryMethod: string;
  booked: boolean;
  bookedByUserId?: string | null;
}

