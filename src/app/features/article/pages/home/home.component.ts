import {Component, OnInit} from "@angular/core";
import { TimeSlotListConfig } from "../../models/time-slot-list-config.model";
import { AsyncPipe, NgClass, NgForOf } from "@angular/common";
import { TimeSlotsListComponent } from "../../components/time-slots-list.component";
import { RxLet } from "@rx-angular/template/let";
import { IfAuthenticatedDirective } from "../../../../core/auth/if-authenticated.directive";
import {DeliveryService} from "../../services/delivery.service";
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";

@Component({
  selector: "app-home-page",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
  imports: [
    NgClass,
    TimeSlotsListComponent,
    AsyncPipe,
    RxLet,
    NgForOf,
    IfAuthenticatedDirective,
    FormsModule,
    ReactiveFormsModule,
  ],
  standalone: true,
})
export default class HomeComponent implements OnInit {
  listConfig: TimeSlotListConfig = {
    filters: {
      deliveryMethod: "DELIVERY"
    },
  };
  deliveryMethods: string[] = [];
  error = '';
  form: FormGroup;
  selectedDeliveryMethod = "DELIVERY";

  constructor(private deliveryService: DeliveryService, private fb: FormBuilder) {
    this.form = this.fb.group({
      deliveryMethod: ['DELIVERY']
    });
  }

  ngOnInit() {
    this.loadDeliveryMethods();

    this.form.get('deliveryMethod')?.valueChanges.subscribe(value => {
      this.listConfig = {
       filters: {
         deliveryMethod : value,
         page: 0,
         size: 20
       }};
      this.selectedDeliveryMethod = value;
    });
  }

  loadDeliveryMethods(): void {
    this.deliveryService.getDeliveryMethods().subscribe({
      next: (data) => this.deliveryMethods = data,
      error: (error) => {
        this.error = 'Error fetching delivery methods';
        console.error(error);
      }
    });
  }


}
