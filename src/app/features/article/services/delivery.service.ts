import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import {EntityModel} from "../models/entity-model.model";

@Injectable({ providedIn: "root" })
export class DeliveryService {
  constructor(private readonly http: HttpClient) {}

  getDeliveryMethods(): Observable<string[]> {
    return this.http
      .get<EntityModel<string>>(`/api/delivery/methods`)
      .pipe(map((data) => data._embedded.stringList));
  }



}
