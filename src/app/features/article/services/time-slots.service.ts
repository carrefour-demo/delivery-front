import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { TimeSlotListConfig } from "../models/time-slot-list-config.model";
import { TimeSlot } from "../models/time-slot.model";

@Injectable({ providedIn: "root" })
export class TimeSlotsService {
  constructor(private readonly http: HttpClient) {}

  query(
    config: TimeSlotListConfig,
  ): Observable<TimeSlot[]> {

    let params = new HttpParams();

    Object.keys(config.filters).forEach((key) => {
      // @ts-ignore
      params = params.set(key, config.filters[key]);
    });

    return this.http.get<TimeSlot[]>(
      "/api/time-slots",
      { params },
    );
  }


  book(timeSlotId: number, deliveryMethod: string ): Observable<TimeSlot> {
    return this.http
      .post<TimeSlot>(`/api/time-slots/${timeSlotId}/${deliveryMethod}/book`, {})
      .pipe(map((data) => data));
  }


}
